﻿namespace Xilium.CefGlue
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Xilium.CefGlue.Interop;

    public struct CefRectangle
    {
        private int _x;
        private int _y;
        private int _width;
        private int _height;

        public CefRectangle(int x, int y, int width, int height)
        {
            _x = x;
            _y = y;
            _width = width;
            _height = height;
        }

		internal unsafe CefRectangle(cef_rect_t* source)
		{
			_x = source->x;
			_y = source->y;
			_width = source->width;
			_height = source->height;
		}

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }

        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }
    }
}
