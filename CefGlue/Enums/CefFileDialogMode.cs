﻿//
// This file manually written from cef/include/internal/cef_types.h.
// C API name: cef_file_dialog_mode_t.
//
namespace Xilium.CefGlue
{
    /// <summary>
    /// Supported file dialog modes.
    /// </summary>
    public enum CefFileDialogMode
    {
        /// <summary>
        /// Requires that the file exists before allowing the user to pick it.
        /// </summary>
        Open = 0,
        
        /// <summary>
        /// Like Open, but allows picking multiple files to open.
        /// </summary>
        OpenMultiple,

		///
		// Like Open, but selects a folder to open.
		///
		OpenFolder,
        
        /// <summary>
        /// Allows picking a nonexistent file, and prompts to overwrite if the file
        /// already exists.
        /// </summary>
        Save,


		///
		// General mask defining the bits used for the type values.
		///
		TypeMask = 0xFF,

		// Qualifiers.
		// Any of the type values above can be augmented by one or more qualifiers.
		// These qualifiers further define the dialog behavior.

		///
		// Prompt to overwrite if the user selects an existing file with the Save
		// dialog.
		///
		OverwritePromptFlag = 0x01000000,

		///
		// Do not display read-only files.
		///
		HideReadOnlyFlag = 0x02000000,

    }
}
